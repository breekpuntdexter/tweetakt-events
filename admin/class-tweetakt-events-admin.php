<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin
 * @author     Studio Breekpunt <info@studiobreek.nl>
 */
class Tweetakt_Events_Admin
{

    /**
     * The default users id
     *
     * @since    1.0.0
     */
    private $default_user_id;

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tweetakt_Events_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tweetakt_Events_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/tweetakt-events-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Tweetakt_Events_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Tweetakt_Events_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/tweetakt-events-admin.js', array('jquery'), $this->version, false);
    }

    public function create_default_tweetakt_user_if_not_exists()
    {
        $username = 'tweetakt';
        $user_id = username_exists($username);
        if (!$user_id) {
            $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
            $user_id = wp_create_user($username, $random_password);
            $user_id = wp_update_user(array('ID' => $user_id, 'display_name' => "Tweetakt"));
        }

        $this->default_user_id = $user_id;
    }

    /**
     *
     * @since    1.0.0
     */
    public function add_active_tickets_sync_button_to_admin_toolbar($wp_admin_bar)
    {
        $wp_admin_bar->add_node(array(
            'parent' => '',
            'id' => 'active-tickets-sync',
            'title' => '<span class="active-tickets-sync">' . __('Sync Events with Active Tickets') . '</span>',
            'href' => esc_url(admin_url() . 'admin.php?action=active_tickets_sync'),
        ));
    }

    /**
     * TODO: Handle errors in request
     *
     * @since    1.0.0
     */
    public function active_tickets_sync()
    {
        // Do your stuff here
        $response = wp_remote_get("https://tweetakt.nl/at/fetch/2018");

        if (is_wp_error($response) || !isset($response['body'])) {
            return;
        }
        // bad response

        // the good stuff
        $body = wp_remote_retrieve_body($response);

        if (is_wp_error($body)) {
            return;
        }
        // bad body

        // decode the data
        $data = json_decode($body, true);

        if (!$data || empty($data)) {
            return;
        }
        // bad data

        $i = 0;
        foreach ($data as $event) {
            if (!current_user_can('publish_posts')) {
                echo 'current user cannot publish posts';
                break;
            }

            $post_id = $event['id'];
            $title = sanitize_text_field(wp_strip_all_tags($event['title'])); // remove any junk
            $title = esc_html(wp_unslash($title));

            $artist = sanitize_text_field(wp_strip_all_tags($event['artist']));
            $artist = esc_html(wp_unslash($artist));
            $artist_name = sanitize_title_with_dashes($artist);

            $artist_id = username_exists($artist_name);

            if (!$artist_id) {
                if (empty($artist_name)) {
                    $artist_id = $this->default_user_id;
                } else {
                    $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
                    $artist_id = wp_create_user($artist_name, $random_password);
                    $artist_id = wp_update_user(array('ID' => $artist_id, 'display_name' => $artist));
                }
            }

            $slug = sanitize_title_with_dashes($title); // converts to a usable post_name

            $post = array(
                'post_name' => $slug,
                'post_title' => $title,
                'post_type' => 'event',
                'post_author' => $artist_id,
            );

            $performances = array();

            foreach ($event['performances'] as $performance) {
                $location_title = sanitize_text_field(wp_strip_all_tags($performance['location']));
                $location_title = esc_html(wp_unslash($location_title));
                $location_slug = sanitize_title_with_dashes($location_title);

                $location = get_page_by_title($location_title, null, 'location');

                if ($location == null) {
                    $location_data = array(
                        'post_name' => $location_slug,
                        'post_title' => $location_title,
                        'post_type' => 'location',
                        'post_author' => $this->default_user_id,
                    );

                    if(null == get_post_status(1000000)) {
                        $location_data['import_id'] = 1000000;
                    }

                    $location_id = wp_insert_post($location_data);
                } else {
                    $location_id = $location->ID;
                }

                $performance_info = array(
                    'location' => $location_id,
                    'id' => $performance['id'],
                    'start' => $performance['startTime'],
                    'end' => $performance['endTime'],
                    'websale' => $performance['websale'],
                    'available' => $performance['available'],
                );

                array_push($performances, $performance_info);
            }

            $post_status = get_post_status($post_id);

            // If the page doesn't already exist, then create it (by title & slug)
            if (null == $post_status) {
                $post['import_id'] = $post_id;

                // Set the post ID so that we know the post was created successfully
                $created_post_id = wp_insert_post($post);
            } else {
                // Arbitrarily use -2 to indicate that the page with the title already exists
                $post['ID'] = $post_id;
                $post['post_status'] = $post_status;

                // Set the post ID so that we know the post was created successfully
                $created_post_id = wp_insert_post($post);

                // echo 'Updated new post: ' . $created_post_id . ' + ' . $post_id;

            } // end if

            update_post_meta(
                $created_post_id,
                'minimum_age',
                intval($event['minimumAge'])
            );

            update_post_meta(
                $created_post_id,
                'performances',
                $performances
            );

            $event_meta = get_post_meta($created_post_id);
            $tickets_saved = isset($event_meta['tickets'])
            ? unserialize($event_meta['tickets'][0])
            : array();

            $tickets = array();
            foreach ($event['tickets'] as $ticket) {
                $show_on_site = isset($tickets_saved[sanitize_title_with_dashes($ticket['type'])])
                ? $tickets_saved[sanitize_title_with_dashes($ticket['type'])]['show_on_site']
                : false;

                $ticket['show_on_site'] = $show_on_site;
                $tickets[sanitize_title_with_dashes($ticket['type'])] = $ticket;
            }

            update_post_meta(
                $created_post_id,
                'tickets',
                $tickets
            );
        }

        wp_redirect(admin_url() . 'edit.php?post_type=event');
        exit();
    }

    /**
     *
     * @since    1.0.0
     */
    public function tweetakt_events_add_event_custom_post_type()
    {
        $labels = array(
            'name' => __('Events'),
            'singular_name' => __('Event'),
            'add_new' => __('Add New Event'),
            'add_new_item' => __('Add New Event'),
            'edit_item' => __('Edit Event'),
            'new_item' => __('New Event'),
            'all_items' => __('All Events'),
            'view_item' => __('View Event'),
            'search_items' => __('Search Events'),
        );

        // TODO: Add Description
        $args = array(
            'labels' => $labels,
            'description' => 'ja komt later wel',
            'public' => true,
            'menu_position' => 5,
            'supports' => array('title', 'editor', 'thumbnail', 'author', 'excerpt'),
            'has_archive' => false,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'query_var' => 'event',
            'menu_icon' => 'dashicons-calendar',
            'show_in_rest' => true,
            'capability_type' => 'post',
            'capabilities' => array(
                'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
            ),
            'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
        );

        register_post_type('event', $args);
    }

    /**
     *
     * @since    1.0.0
     */
    public function tweetakt_events_add_location_custom_post_type()
    {
        $labels = array(
            'name' => __('Locations'),
            'singular_name' => __('Location'),
            'add_new' => __('Add New Location'),
            'add_new_item' => __('Add New Location'),
            'edit_item' => __('Edit Location'),
            'new_item' => __('New Location'),
            'all_items' => __('All Locations'),
            'view_item' => __('View Location'),
            'search_items' => __('Search Locations'),
        );

        // TODO: Add Description
        $args = array(
            'labels' => $labels,
            'description' => 'ja komt later wel',
            'public' => true,
            'menu_position' => 6,
            'supports' => array('title', 'editor', 'thumbnail', 'author'),
            'taxonomies' => array('genre', 'accessibility'),
            'has_archive' => false,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'query_var' => 'location',
            'menu_icon' => 'dashicons-store',
            'show_in_rest' => true,
            'capability_type' => 'post',
            'capabilities' => array(
                'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
            ),
            'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
        );

        register_post_type('location', $args);
    }

    public function register_genre_taxonomy()
    {
        $labels = array(
            'name' => _x('Genres', 'taxonomy general name', 'textdomain'),
            'singular_name' => _x('Genre', 'taxonomy singular name', 'textdomain'),
            'search_items' => __('Search Genres', 'textdomain'),
            'all_items' => __('All Genres', 'textdomain'),
            'parent_item' => __('Parent Genre', 'textdomain'),
            'parent_item_colon' => __('Parent Genre:', 'textdomain'),
            'edit_item' => __('Edit Genre', 'textdomain'),
            'update_item' => __('Update Genre', 'textdomain'),
            'add_new_item' => __('Add New Genre', 'textdomain'),
            'new_item_name' => __('New Genre Name', 'textdomain'),
            'menu_name' => __('Genre', 'textdomain'),
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'show_in_rest' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_options',
                'edit_terms' => 'manage_options',
                'delete_terms' => 'manage_options',
                'assign_terms' => 'edit_posts',
            ),
            'public' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        );

        register_taxonomy('genre', array('event'), $args);
    }

    public function register_accessibility_taxonomy()
    {
        $labels = array(
            'name' => _x('Accessibility', 'taxonomy general name', 'textdomain'),
            'singular_name' => _x('Accessibility', 'taxonomy singular name', 'textdomain'),
            'search_items' => __('Search Accessibility', 'textdomain'),
            'all_items' => __('All Accessibility', 'textdomain'),
            'parent_item' => __('Parent Genre', 'textdomain'),
            'parent_item_colon' => __('Parent Genre:', 'textdomain'),
            'edit_item' => __('Edit Accessibility', 'textdomain'),
            'update_item' => __('Update Accessibility', 'textdomain'),
            'add_new_item' => __('Add New Accessibility', 'textdomain'),
            'new_item_name' => __('New Accessibility Name', 'textdomain'),
            'menu_name' => __('Accessibility', 'textdomain'),
        );

        $args = array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'query_var' => true,
            'show_in_rest' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_options',
                'edit_terms' => 'manage_options',
                'delete_terms' => 'manage_options',
                'assign_terms' => 'edit_posts',
            ),
            'public' => true,
            'show_in_nav_menus' => false,
            'show_tagcloud' => false,
        );

        register_taxonomy('accessibility', array('event'), $args);
    }

    public function allowed_blocks($allowed_blocks, $post) {
        if( $post->post_type === 'event' ) {
            $allowed_blocks = array(
                'core/image',
                'core/paragraph',
                'core/heading',
                'core/columns',
                'core/quote',
                'core/video',
            );
        }
     
        return $allowed_blocks;
    }

    public function add_related_events_meta_box()
    {
        add_meta_box(
            'tweetakt_events_related_events_meta',
            __('Related Events', $this->plugin_name),
            array($this, 'display_related_events_meta_box'),
            'event'
        );
    }

    public function display_related_events_meta_box($event)
    {
        $event_meta = get_post_meta($event->ID);

        $related_events = isset($event_meta['related_events'])
        ? unserialize($event_meta['related_events'][0])
        : array();

        include_once 'partials/tweetakt-events-admin-related-events-meta-box-display.php';
    }

    public function save_related_events_meta_box($event_id)
    {
        if (!isset($_POST[$this->plugin_name . '_related_events_meta_nonce'])) {
            return;
        }

        $nonce = $_POST[$this->plugin_name . '_related_events_meta_nonce'];
        if (!wp_verify_nonce($nonce, $this->plugin_name . '_save_related_events_meta')) {
            return;
        }

        $related_events = isset($_POST['related_events']) ? $_POST['related_events'] : array();

        update_post_meta(
            $event_id,
            'related_events',
            $related_events
        );
    }

    public function add_extra_info_meta_box()
    {
        add_meta_box(
            'tweetakt_events_extra_info_meta',
            __('Information', $this->plugin_name),
            array($this, 'display_extra_info_meta_box'),
            'event',
            'side'
        );
    }

    public function display_extra_info_meta_box($event)
    {
        $event_meta = get_post_meta($event->ID);

        $extra_info = isset($event_meta['extra_info'])
        ? $event_meta['extra_info'][0]
        : "";

        $intro_text = isset($event_meta['intro_text'])
        ? $event_meta['intro_text'][0]
        : "";

        include_once 'partials/tweetakt-events-admin-extra-info-meta-box-display.php';
    }

    public function save_extra_info_meta_box($event_id)
    {
        if (!isset($_POST[$this->plugin_name . '_extra_info_meta_nonce'])) {
            return;
        }

        $nonce = $_POST[$this->plugin_name . '_extra_info_meta_nonce'];
        if (!wp_verify_nonce($nonce, $this->plugin_name . '_save_extra_info_meta')) {
            return;
        }

        $extra_info = isset($_POST[$this->plugin_name . '-extra_info'])
        ? $_POST[$this->plugin_name . '-extra_info']
        : '';

        update_post_meta(
            $event_id,
            'extra_info',
            $extra_info
        );

        $intro_text = isset($_POST[$this->plugin_name . '-intro_text'])
        ? $_POST[$this->plugin_name . '-intro_text']
        : '';

        update_post_meta(
            $event_id,
            'intro_text',
            $intro_text
        );
    }

    public function add_event_meta_box()
    {
        add_meta_box(
            'tweetakt_events_meta',
            __('Active Tickets Info', $this->plugin_name),
            array($this, 'display_meta_box'),
            'event',
            'side'
        );
    }

    public function display_meta_box($event)
    {
        include_once 'partials/tweetakt-events-admin-meta-box-display.php';
    }

    public function add_performances_meta_boxes($post_type, $event)
    {
        $event_meta = get_post_meta($event->ID);

        $performances = isset($event_meta['performances'])
        ? unserialize($event_meta['performances'][0])
        : array();

        foreach ($performances as $performance) {
            $meta_box_id = $performance['id'];
            $parsed_start_date = date_parse($performance['start']);

            $start_date = new DateTime($performance['start']);
            $end_date = new DateTime($performance['end']);
            $meta_box_title = $start_date->format('j M Y H:i') . ' - ' . $end_date->format('H:i');

            add_meta_box(
                __('tweetakt_events_performance' . $meta_box_id),
                __($meta_box_title, $this->plugin_name),
                array($this, 'display_performance_meta_box'),
                'event',
                'side',
                'default',
                $performance
            );
        }
    }

    public function display_performance_meta_box($event, $performance)
    {
        $performance = $performance['args'];

        $performance_id = $performance['id'];

        $location_id = $performance['location'];
        $location = get_post($location_id)->post_title;
        $websale = $performance['websale'] ? 'yes' : 'no';
        $available = $performance['available'] ? 'yes' : 'no';

        $event_meta = get_post_meta($event->ID);

        $premieres = isset($event_meta['premieres'])
        ? unserialize($event_meta['premieres'][0])
        : array();

        $tryouts = isset($event_meta['tryouts'])
        ? unserialize($event_meta['tryouts'][0])
        : array();

        include 'partials/tweetakt-events-admin-performance_meta-box-display.php';
    }

    public function save_performance_is_premiere($event_id)
    {   
        if (!isset($_POST[$this->plugin_name . '_tickets_meta_nonce'])) {
            return;
        }

        $nonce = $_POST[$this->plugin_name . '_tickets_meta_nonce'];
        if (!wp_verify_nonce($nonce, $this->plugin_name . '_save_tickets_meta')) {
            return;
        }

        $premieres = isset($_POST['performance-premiere'])
        ? $_POST['performance-premiere']
        : array();

        update_post_meta(
            $event_id,
            'premieres',
            $premieres
        );

        $tryouts = isset($_POST['performance-tryout'])
        ? $_POST['performance-tryout']
        : array();

        update_post_meta(
            $event_id,
            'tryouts',
            $tryouts
        );

    }

    public function add_tickets_meta_box()
    {
        add_meta_box(
            'tweetakt_events_tickets_meta',
            __('Tickets', $this->plugin_name),
            array($this, 'display_tickets_meta_box'),
            'event',
            'side'
        );
    }

    public function display_tickets_meta_box($event)
    {
        $event_meta = get_post_meta($event->ID);

        $tickets = isset($event_meta['tickets'])
        ? unserialize($event_meta['tickets'][0])
        : array();

        include_once 'partials/tweetakt-events-admin-tickets-meta-box-display.php';
    }

    public function save_tickets_meta_box($event_id)
    {
        if (!isset($_POST[$this->plugin_name . '_tickets_meta_nonce'])) {
            return;
        }

        $nonce = $_POST[$this->plugin_name . '_tickets_meta_nonce'];
        if (!wp_verify_nonce($nonce, $this->plugin_name . '_save_tickets_meta')) {
            return;
        }

        $event_meta = get_post_meta($event_id);

        $tickets_saved = isset($event_meta['tickets'])
        ? unserialize($event_meta['tickets'][0])
        : array();

        // Save location description
        foreach ($tickets_saved as $ticket_name => $ticket) {
            $checked = isset($_POST[$ticket_name]) ? true : false;

            $tickets_saved[$ticket_name]['show_on_site'] = $checked;
        }

        update_post_meta(
            $event_id,
            'tickets',
            $tickets_saved
        );
    }
}
