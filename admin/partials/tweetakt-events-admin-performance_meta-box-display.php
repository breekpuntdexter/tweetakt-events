<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin/partials
 */

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="components-panel__row">
    <span><strong><a href="<?php echo get_edit_post_link($location_id); ?>"><?php echo $location; ?></a></strong></span>
</div>

<div class="components-panel__row">
    <span>Websale</span>
    <div>
        <span><strong><?php echo $websale; ?></span></strong>
    </div>
</div>

<div class="components-panel__row">
    <span>Available</span>
    <div>
        <span><strong><?php echo $available; ?></span></strong>
    </div>
</div>

<div class="components-panel__row">
    <span>Premiere</span>
    <div>
        <input
            type="checkbox"
            name="performance-premiere[]"
            value="<?php echo $performance_id; ?>"
            <?php echo in_array($performance_id, $premieres) ? 'checked' : ''; ?>
        />
    </div>
</div>

<div class="components-panel__row">
    <span>Tryout</span>
    <div>
        <input
            type="checkbox"
            name="performance-tryout[]"
            value="<?php echo $performance_id; ?>"
            <?php echo in_array($performance_id, $tryouts) ? 'checked' : ''; ?>
        />
    </div>
</div>