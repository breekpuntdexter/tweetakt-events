<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
wp_nonce_field($this->plugin_name . '_save_related_events_meta', $this->plugin_name . '_related_events_meta_nonce');
?>

<?php foreach($related_events as $event_id) { 
  $event = get_post($event_id);
?>

<div class="components-panel__row" id="<?php echo $event_id; ?>">
    <span><?php echo $event->post_title; ?></span>
    <div>
        <input
            type="checkbox" 
            id="<?php echo $event->post_name; ?>"
            name="related_events[]"
            value="<?php echo $event_id; ?>"
            checked
        />
    </div>
</div>

<?php }?>

<div class="components-panel__row">
  <div>
    <p><strong>Only lists published events</strong></p>
    <input
        type="text"
        id="tweetakt_events_search_related_events"
        name="<?php echo $this->plugin_name; ?>-search-related-events"
        placeholder="Search events"
    />
    <button id="search_related_events">Search</button>
  </div>
</div>
<div class="components-panel__row">
  <div id="search_results">
  </div>
</div>
