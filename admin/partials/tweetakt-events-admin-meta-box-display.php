<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin/partials
 */

$event_meta = get_post_meta($event->ID);
$minimum_age = $event_meta['minimum_age'][0];

if (intval($minimum_age) == 0) {
    $minimum_age = 'All ages';
}

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="components-panel__row">
    <span>Minimum Age</span>
    <div>
        <span><strong><?php echo $minimum_age; ?></span></strong>
    </div>
</div>

    <!-- // $location = get_post($performance['location']);
    // echo $location->post_title; -->
