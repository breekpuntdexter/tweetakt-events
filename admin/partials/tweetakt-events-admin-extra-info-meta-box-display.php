<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
wp_nonce_field($this->plugin_name . '_save_extra_info_meta', $this->plugin_name . '_extra_info_meta_nonce');
?>

<div class="components-panel__row">
  <div>
    <span><strong>Intro Text</strong></span>
    <br />
    <textarea
        type="text"
        id="<?php echo $this->plugin_name; ?>-intro_text"
        name="<?php echo $this->plugin_name; ?>-intro_text"
        placeholder="Introduction at the top of the page"
        rows="5"
    ><?php echo $intro_text; ?></textarea>
  </div>
</div>

<div class="components-panel__row">
  <div>
    <span><strong>Extra information</strong></span>
    <input
        type="text"
        id="<?php echo $this->plugin_name; ?>-extra_info"
        name="<?php echo $this->plugin_name; ?>-extra_info"
        value="<?php echo $extra_info; ?>"
        placeholder="Bring an umbrella"
    />
  </div>
</div>
