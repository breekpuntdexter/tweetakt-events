<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://studiobreek.nl
 * @since      1.0.0
 *
 * @package    Tweetakt_Events
 * @subpackage Tweetakt_Events/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
wp_nonce_field($this->plugin_name . '_save_tickets_meta', $this->plugin_name . '_tickets_meta_nonce');
?>

<?php foreach($tickets as $ticket) { ?>

<div class="components-panel__row">
    <span><?php echo $ticket['type']; ?>: €<?php echo number_format($ticket['price'], 2); ?></span>
    <div>
        <input
            type="checkbox" 
            id="<?php echo $this->plugin_name; ?>-tickets-<?php echo $ticket['type']; ?>"
            name="<?php echo sanitize_title_with_dashes($ticket['type']); ?>"
            value="1"
            <?php echo $ticket['show_on_site'] ? 'checked' : ''; ?>
        />
    </div>
</div>

<?php }?>