<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://studiobreek.nl
 * @since             1.0.0
 * @package           Tweetakt_Events
 *
 * @wordpress-plugin
 * Plugin Name:       Tweetakt Events
 * Plugin URI:        https://bitbucket.org/breekpuntdexter/tweetakt-events
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Studio Breekpunt
 * Author URI:        http://studiobreek.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       tweetakt-events
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'TWEETAKT_EVENTS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-tweetakt-events-activator.php
 */
function activate_tweetakt_events() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tweetakt-events-activator.php';
	Tweetakt_Events_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-tweetakt-events-deactivator.php
 */
function deactivate_tweetakt_events() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tweetakt-events-deactivator.php';
	Tweetakt_Events_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_tweetakt_events' );
register_deactivation_hook( __FILE__, 'deactivate_tweetakt_events' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-tweetakt-events.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_tweetakt_events() {

	$plugin = new Tweetakt_Events();
	$plugin->run();

}
run_tweetakt_events();
